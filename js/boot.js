var
    game = new Phaser.Game(800, 600, Phaser.AUTO, 'game'),
    Boot = function () { },
    gameOptions = {
        //Audio
        playSound: true,
        playMusic: true,
        //Board
        screenWidth: 800,
        screenHeight: 600,
        dashSize: 5,
        //paddles
        paddleLeft_x: 34,
        paddleRight_x: 758,
        paddleVelocity: 350,
        paddleSegmentsMax: 4,
        paddleSegmentHeight: 4,
        paddleSegmentAngle: 15,
        //ball
        ballVelocity: 300,
        ballRandomStartingAngleLeft: [-120, 120],
        ballRandomStartingAngleRight: [-60, 60],
        ballStartDelay: 3.5,
        ballVelocityIncrement: 25,
        ballReturnCount: 4,
        //score
        scoreToWin: 5,
        //enemy
        versus: false
    },
    backgroundGraphics,
    scoreLeft = 0,
    scoreRight = 0,
    ballOnScreenCenter = true,
    ballReturnCount,
    musicPlayer,
    sndBallHit,
    sndBallBounce,
    missedSide;

Boot.prototype = {
    preload: function () {
        game.load.image('stars', 'assets/images/stars.jpg');
        game.load.image('loading', 'assets/images/loading.png');
        // game.load.image('brand', 'assets/images/logo.png');
        game.load.script('utils', 'utils.js');
        game.load.script('load', 'load.js');
    },

    create: function () {
        game.state.add('load', Load);
        game.state.start('load');
    }
};

game.state.add('boot', Boot);
game.state.start('boot');