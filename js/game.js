var Game = function () { };

Game.prototype = {

    preload: function () {

    },

    create: function () {
        timer = game.time.create(false);
        this.initGraphics();
        this.initPhysics();
        this.initSounds();
        this.resetBall();
        cursors = game.input.keyboard.createCursorKeys();
        // input
        game.input.keyboard.addKeyCapture([
            Phaser.Keyboard.Z,
            Phaser.Keyboard.S
        ]);
        pauseKey = this.input.keyboard.addKey(Phaser.Keyboard.P);
        // game.time.events.add(Phaser.Timer.SECOND * gameOptions.ballStartDelay, this.releaseBall, this);
        game.input.onDown.add(this.unpause, self);
    },

    initGraphics: function () {
        backgroundGraphics = game.add.graphics(0, 0);
        backgroundGraphics.lineStyle(2, 0xFFFFFF, 1);
        for (var y = 0; y < gameOptions.screenHeight; y += gameOptions.dashSize * 2) {
            backgroundGraphics.moveTo(game.world.centerX, y);
            backgroundGraphics.lineTo(game.world.centerX, y + gameOptions.dashSize);
        }
        ball = game.add.sprite(game.world.centerX, game.world.centerY, 'ball');
        ball.anchor.set(0.5, 0.5);
        paddles = game.add.group();
        paddleLeft = paddles.create(gameOptions.paddleLeft_x, game.world.centerY, 'paddle');
        paddleRight = paddles.create(gameOptions.paddleRight_x, game.world.centerY, 'paddle');
        paddleLeft.anchor.set(0.5, 0.5);
        paddleRight.anchor.set(0.5, 0.5);
        scoreTextLeft = game.add.text(gameOptions.screenWidth * 0.25, 10, "0", { fontSize: '32px', fill: '#fff' });
        scoreTextRight = game.add.text(gameOptions.screenWidth * 0.75, 10, "0", { fontSize: '32px', fill: '#fff' });
        scoreTextLeft.anchor.set(0.5, 0);
        scoreTextRight.anchor.set(0.5, 0);
        timerText = game.add.text(gameOptions.screenWidth * 0.5, gameOptions.screenHeight * 0.35, "0", { font: '62px Arial', fill: '#ff0' });
        timerText.anchor.set(0.5, 0);
    },

    initPhysics: function () {
        //physics
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.physics.enable([paddles, ball], Phaser.Physics.ARCADE);
        game.physics.arcade.checkCollision.left = false;
        game.physics.arcade.checkCollision.right = false;
        //ball
        ball.checkWorldBounds = true;
        ball.enableBody = true;
        ball.body.collideWorldBounds = true;
        ball.body.immovable = true;
        ball.body.bounce.set(1);
        ball.events.onOutOfBounds.add(this.ballOutOfBounds, this);
        ball.body.velocity.setTo(0, 0);
        //paddles
        paddles.enableBody = true;
        paddles.setAll('checkWorldBounds', true);
        paddles.setAll('body.collideWorldBounds', true);
        paddles.setAll('body.immovable', true);
    },

    initSounds: function () {
        sndBallHit = game.add.audio('hit');
        sndBallBounce = game.add.audio('bounce');
        sndBallMissed = game.add.audio('miss');
    },

    update: function () {
        this.movePaddleLeft();
        this.movePaddleRight();
        game.physics.arcade.overlap(ball, paddles, this.collideWithPaddle, null, this);
        if (ball.body.blocked.up || ball.body.blocked.down || ball.body.blocked.left || ball.body.blocked.right) {
            if (gameOptions.playSound) { sndBallBounce.play() }
        }
        if (game.input.keyboard.isDown(Phaser.Keyboard.P)) {
            this.setPause();
        }
        timerText.text = '';
        if (timer.running) {
            timerText.alpha = 1;
            if (timer.duration / 1000 < 0.5) {
                timerText.text = 'GO!';
            } else {
                timerText.text = Math.round(timer.duration / 1000);
            }
            game.add.tween(timerText).to({ alpha: 0.1 }, 500, Phaser.Easing.None, true);
        }
    },

    render: function () {
        //
    },

    movePaddleLeft: function () {
        if (game.input.keyboard.isDown(Phaser.Keyboard.Z)) {
            paddleLeft.body.velocity.y = -gameOptions.paddleVelocity;
        }
        else if (game.input.keyboard.isDown(Phaser.Keyboard.S)) {
            paddleLeft.body.velocity.y = gameOptions.paddleVelocity;
        }
        else {
            paddleLeft.body.velocity.setTo(0, 0);
        }
    },

    collideWithPaddle: function (ball, paddle) {
        if (gameOptions.playSound) { sndBallHit.play(); }
        var returnAngle;
        var segmentHit = Math.floor((ball.y - paddle.y) / gameOptions.paddleSegmentHeight);

        if (segmentHit >= gameOptions.paddleSegmentsMax) {
            segmentHit = gameOptions.paddleSegmentsMax - 1;
        } else if (segmentHit <= -gameOptions.paddleSegmentsMax) {
            segmentHit = -(gameOptions.paddleSegmentsMax - 1);
        }

        if (paddle.x < gameOptions.screenWidth * 0.5) {
            returnAngle = segmentHit * gameOptions.paddleSegmentAngle;

        } else {
            returnAngle = 180 - (segmentHit * gameOptions.paddleSegmentAngle);
            if (returnAngle > 180) {
                returnAngle -= 360;
            }
        }
        game.physics.arcade.velocityFromAngle(returnAngle, ballVelocity, ball.body.velocity);
        ballVelocity += gameOptions.ballVelocityIncrement;
    },

    movePaddleRight: function () {
        if (gameOptions.versus) {
            if (cursors.up.isDown) {
                paddleRight.body.velocity.y = -gameOptions.paddleVelocity;
            }
            else if (cursors.down.isDown) {
                paddleRight.body.velocity.y = gameOptions.paddleVelocity;
            }
            else {
                paddleRight.body.velocity.setTo(0, 0);
            }
        }
        else {
            if (ball.body.y < paddleRight.body.y - 5) {
                paddleRight.body.velocity.y = -gameOptions.paddleVelocity;
            }
            else if (ball.body.y > paddleRight.body.y + 5) {
                paddleRight.body.velocity.y = gameOptions.paddleVelocity;
            }
            else {
                paddleRight.body.velocity.setTo(0, 0);
            }
        }
    },

    ballOutOfBounds: function () {
        if (gameOptions.playSound) { sndBallMissed.play(); }

        if (ball.x < 0) {
            missedSide = 'left';
            scoreRight++;
        }
        else if (ball.x > gameOptions.screenWidth) {
            missedSide = 'right';
            scoreLeft++;
        }

        this.updateScoreText();

        if (scoreLeft >= gameOptions.scoreToWin || scoreRight >= gameOptions.scoreToWin) {
            this.resetBall();
            this.resetScore();
            game.state.start("gameOver");
        } else {
            this.resetBall();
        }
    },

    releaseBall: function () {
        timer.stop(false);
        ballVelocity = gameOptions.ballVelocity;
        ballReturnCount = 0;
        var randomAngle = game.rnd.pick(gameOptions.ballRandomStartingAngleRight.concat(gameOptions.ballRandomStartingAngleLeft));

        if (missedSide == 'right') {
            randomAngle = game.rnd.pick(gameOptions.ballRandomStartingAngleRight);
        } else if (missedSide == 'left') {
            randomAngle = game.rnd.pick(gameOptions.ballRandomStartingAngleLeft);
        }
        game.physics.arcade.velocityFromAngle(randomAngle, ballVelocity, ball.body.velocity);
    },

    updateScoreText: function () {
        scoreTextLeft.text = scoreLeft;
        scoreTextRight.text = scoreRight;
    },

    resetBall: function () {
        timer.loop(Phaser.Timer.SECOND * gameOptions.ballStartDelay, this.releaseBall, this);
        timer.start();
        ballOnScreenCenter = true;
        ball.body.velocity.setTo(0, 0);
        ball.reset(game.world.centerX, game.world.centerY);
        paddleLeft.body.y = game.world.centerY;
        paddleRight.body.y = game.world.centerY;

    },

    resetScore: function () {
        scoreLeft = 0;
        scoreRight = 0;
        this.updateScoreText();
    },

    setPause: function () {
        game.paused = true;
        console.log('pause');
    },

    unpause: function (event) {
        if (game.paused) {
            console.log('unpause');
            // Unpause the game
            game.paused = false;
        }
    },
}    